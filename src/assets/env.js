(function(window) {
    window["env"] = window["env"] || {};

    // Environment variables
    window["env"]["pageHeader"] = "My Services";
    window["env"]["pageHeaderIcon"] = "/assets/logo/LinkLion.png";
    window["env"]["dataSource"] = "/assets/data/links.json";
    window["env"]["debug"] = true;
})(this);
